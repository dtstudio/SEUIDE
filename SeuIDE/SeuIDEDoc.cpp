// SeuIDEDoc.cpp : CSeuIDEDoc 类的实现
//

#include "stdafx.h"
#include "SeuIDE.h"

#include "SeuIDEDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSeuIDEDoc

IMPLEMENT_DYNCREATE(CSeuIDEDoc, CDocument)

BEGIN_MESSAGE_MAP(CSeuIDEDoc, CDocument)
END_MESSAGE_MAP()


// CSeuIDEDoc 构造/析构

CSeuIDEDoc::CSeuIDEDoc()
{
	// TODO: 在此添加一次性构造代码

}

CSeuIDEDoc::~CSeuIDEDoc()
{
}

BOOL CSeuIDEDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)

	return TRUE;
}




// CSeuIDEDoc 序列化

void CSeuIDEDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}


// CSeuIDEDoc 诊断

#ifdef _DEBUG
void CSeuIDEDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSeuIDEDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CSeuIDEDoc 命令
