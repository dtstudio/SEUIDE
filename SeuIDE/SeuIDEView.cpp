// SeuIDEView.cpp : CSeuIDEView 类的实现
//

#include "stdafx.h"
#include "SeuIDE.h"

#include "SeuIDEDoc.h"
#include "SeuIDEView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSeuIDEView

IMPLEMENT_DYNCREATE(CSeuIDEView, CView)

BEGIN_MESSAGE_MAP(CSeuIDEView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CSeuIDEView 构造/析构

CSeuIDEView::CSeuIDEView()
{
	// TODO: 在此处添加构造代码

}

CSeuIDEView::~CSeuIDEView()
{
}

BOOL CSeuIDEView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CSeuIDEView 绘制

void CSeuIDEView::OnDraw(CDC* /*pDC*/)
{
	CSeuIDEDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CSeuIDEView 打印

BOOL CSeuIDEView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CSeuIDEView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CSeuIDEView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CSeuIDEView 诊断

#ifdef _DEBUG
void CSeuIDEView::AssertValid() const
{
	CView::AssertValid();
}

void CSeuIDEView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSeuIDEDoc* CSeuIDEView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSeuIDEDoc)));
	return (CSeuIDEDoc*)m_pDocument;
}
#endif //_DEBUG


// CSeuIDEView 消息处理程序
